/**
 * Created by goat on 2015-02-28.
 */


var AppDispatcher = require('../dispatcher/AppDispatcher');
var GroceryListConstants = require('../constants/GroceryListConstants');


var GroceryListActions = {

  loadFromStorage: function(data)
  {
    AppDispatcher.dispatch({
      actionType: GroceryListConstants.GROCERY_LIST_LOAD_FROM_STORAGE,
      data: data
    })
  },


  create: function(text) {
    AppDispatcher.dispatch({
      actionType: GroceryListConstants.GROCERY_LIST_CREATE,
      text: text
    });
  },


  updateItem: function(id, text) {
    AppDispatcher.dispatch({
      actionType: GroceryListConstants.GROCERY_LIST_UPDATE_ITEM,
      id: id,
      text: text
    });
  },


  toggleComplete: function(id, complete) {
    AppDispatcher.dispatch({
      actionType: GroceryListConstants.GROCERY_LIST_SET_COMPLETE,
      id: id,
      complete: complete
    });
  },


  toggleCompleteAll: function() {
    AppDispatcher.dispatch({
      actionType: GroceryListConstants.GROCERY_LIST_TOGGLE_COMPLETE_ALL
    });
  },


  destroy: function(id) {
    AppDispatcher.dispatch({
      actionType: GroceryListConstants.GROCERY_LIST_DESTROY,
      id: id
    });
  },


  destroyCompleted: function() {
    AppDispatcher.dispatch({
      actionType: GroceryListConstants.GROCERY_LIST_DESTROY_COMPLETED
    });
  }


};



module.exports = GroceryListActions;
