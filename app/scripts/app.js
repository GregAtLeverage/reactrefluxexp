var Router = require('./router');
var GroceryListActions = require('./actions/GroceryListActions');
var Storage = require('./storage');

Router.start();


GroceryListActions.loadFromStorage(Storage.getStore());


