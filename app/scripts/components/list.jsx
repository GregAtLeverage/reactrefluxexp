var React = require('react');
var cx = require('react/lib/cx');

var evtbus = require('../eventbus');

var GroceryListActions = require('../actions/GroceryListActions');
var GroceryListStore = require('../stores/GroceryListStore');


var ListControlBar = require('./list_control_bar.jsx');


evtbus.on("evt1", function(arg){
  console.log("event received...", arg);
});




var ListItemInput = React.createClass({

  getInitialState: function(){
    return {
      inputValue: this.props.itemValue,
      itemKey: this.props.itemKey
    };
  },

  on_click: function(evt)
  {
    evt.stopPropagation();
  },

  on_keyup: function(evt)
  {
    var input_text = evt.target.value;
    this.setState({inputValue: input_text});

    var keyCode = evt.keyCode;
    if(keyCode == 13){
      this.props.onSave(input_text);
    }
  },

  on_focus: function(evt)
  {
    evt.target.value = evt.target.value;
  },

  render: function()
  {
    return (
      <input type="text"
             className="item-input"
             defaultValue={this.state.inputValue}
             onClick={this.on_click}
             onKeyUp={this.on_keyup}
             onBlur={this.props.onBlur}
             autoFocus={true}
             onFocus={this.on_focus} />
    );
  }

});





var ListItemValue = React.createClass({

  getInitialState: function(){
    return {};
  },

  render: function()
  {
    var classes = cx({
      "item-value": true,
      "complete": this.props.itemComplete
    });


    return (
      <p className={classes}>
      {this.props.itemValue}
      </p>
    );
  }

});






var ListItemToggle = React.createClass({

  getInitialState: function(){
    return {
      complete: this.props.itemComplete
    }
  },

  on_click: function(evt)
  {
    this.props.onToggle( ! this.state.complete );
    this.setState({
      complete: !this.state.complete
    });
  },

  render: function()
  {
    var classes = cx({
      toggle: true,
      complete: this.state.complete
    });

    return (
      <div className={classes}
           onClick={this.on_click} >
      </div>
    );
  }

});






var ListItemButtonGroup = React.createClass({

  getInitialState: function()
  {
    return {
      itemSelected: this.props.itemSelected
    };
  },

  on_click_edit: function(evt)
  {
    evt.stopPropagation();
    this.props.onEdit();
  },

  on_click_remove: function(evt)
  {
    evt.stopPropagation();
    this.props.onRemove();
  },

  render: function()
  {
    var classes = cx({
      "list-item-button-group": true,
      "selected": this.props.itemSelected
    });

    return (
      <div className={classes}>
        <div className="edit-btn" onClick={this.on_click_edit}></div>
        <div className="remove-btn" onClick={this.on_click_remove}></div>
      </div>
    );
  }

});







var ListItem = React.createClass({

  getInitialState: function(){
    return {
      editing: false,
      itemValue: this.props.item.text,
      itemKey: this.props.item.id,
      itemSelected: false,
      itemComplete: this.props.item.complete
    }
  },

  on_li_click: function()
  {
    this.setState({editing: !this.state.editing});
  },

  on_mouseover: function(evt)
  {
    console.log("on mouseover...");
  },

  on_input_change: function(evt)
  {
    this.setState({itemValue: evt.target.value});
  },

  on_keyup: function(evt)
  {
    if(evt.keyCode == 13){
      this.setState({editing: false});
    }
  },

  on_blur: function(evt)
  {
    this.setState({editing: false});
  },

  on_focus: function(evt)
  {
    evt.target.value = evt.target.value;
  },

  on_click_item: function(evt)
  {
    this.setState({itemSelected: !this.state.itemSelected});
  },

  _onSave: function(value)
  {
    this.setState({
      itemValue: value,
      editing: false
    });

    GroceryListActions.updateItem(this.state.itemKey, value);
  },

  _onToggle: function(selected)
  {
    this.setState({itemComplete: selected});
    GroceryListActions.toggleComplete(this.state.itemKey, selected);
  },

  _onEdit: function(evt)
  {
    this.setState({
      itemSelected: false,
      editing: true
    });
  },

  _onRemove: function(evt)
  {
    GroceryListActions.destroy(this.state.itemKey);
  },

  render: function()
  {
    var item = false;
    if(this.state.editing){
      item = <ListItemInput itemValue={this.state.itemValue}
                            itemKey={this.state.itemKey}
                            onSave={this._onSave}
                            onBlur={this.on_blur}/>
    } else {
      item = <ListItemValue itemValue={this.state.itemValue}
                            itemComplete={this.state.itemComplete} />
    }


    var classes = cx({
      "list-item": true,
      "completed": this.props.item.complete
    });

    return (
      <li className={classes} itemKey={this.state.itemKey}>
        <div className="list-item-inner">
          <div className="item-toggle">

            <ListItemToggle itemKey={this.state.itemKey}
                            itemComplete={this.state.itemComplete}
                            onToggle={this._onToggle} />
          </div>
          <div className="item"
               onClick={this.on_click_item}>
            {item}
            <ListItemButtonGroup itemSelected={this.state.itemSelected}
                                 onEdit={this._onEdit}
                                 onRemove={this._onRemove} />
          </div>
        </div>
      </li>
    );
  }

});




var ListExp = React.createClass({

  getInitialState: function()
  {
    return {
      items: []
    };
  },

  componentDidMount: function() {
    GroceryListStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    GroceryListStore.removeChangeListener(this._onChange);
  },

  _onChange: function()
  {
    this.setState({
      items: GroceryListStore.getAllArray()
    });
  },

  render: function()
  {

    var classes = cx({
      "list-component": true,
      "hide-completed": this.props.hideCompleted
    });

    return (
      <ul className={classes}>

      {this.props.items.map(function(item, i){
        return (
          <ListItem idx={i} key={item.id} item={item} />
        )
      }, this)}

      </ul>
    );
  }

});





var AddNewItem = React.createClass({

  getInitialState: function()
  {
    return {
      value: null
    };
  },

  on_keyup:function(evt)
  {
    var input_value = evt.target.value;
    if(input_value.length > 0){

      this.setState({value: input_value});

      if(evt.keyCode == 13){
        GroceryListActions.create(input_value);
        evt.target.value = "";
      }
    }
  },

  render: function()
  {
    return (
      <input type="text"
             className="grocery-item-input"
             placeholder="Grocery Item"
             onKeyUp={this.on_keyup} />
    )
  }

});






var List = React.createClass({

  getInitialState: function()
  {
    return {
      items: [],
      hideCompleted: false
    };
  },

  componentDidMount: function() {
    GroceryListStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    GroceryListStore.removeChangeListener(this._onChange);
  },

  _onChange: function()
  {
    this.setState({
      items: GroceryListStore.getAllArray()
    });
  },


  _onHideCompleted: function(hide)
  {
    console.log("List._onHideCompleted() ---hide---> ", hide);
    this.setState({hideCompleted: hide});
  },


  render: function() {
    return (
      <div>
        <AddNewItem />
        <ListControlBar items={this.state.items}
                        onHideCompleted={this._onHideCompleted}/>

        <ListExp items={this.state.items}
                 hideCompleted={this.state.hideCompleted}/>
      </div>
    );
  }

});




module.exports = List;

