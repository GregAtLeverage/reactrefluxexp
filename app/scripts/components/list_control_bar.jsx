var React = require('react');
var cx = require('react/lib/cx');

var evtbus = require('../eventbus');

var GroceryListActions = require('../actions/GroceryListActions');
var GroceryListStore = require('../stores/GroceryListStore');



var ItemCountWidget = React.createClass({

  render: function()
  {
    return (
      <h3 className="item-count">
      {this.props.itemCount} items
      </h3>
    );
  }

});





var HideCompletedButton = React.createClass({

  getInitialState: function()
  {
    return {
      hide: false
    }
  },

  on_click: function(evt)
  {
    var _hide = !this.state.hide;
    this.setState({hide: _hide});
    this.props.onHideToggle(_hide);
  },


  render: function()
  {
    var classes = cx({
      "hide-completed-btn": true,
      "selected": this.state.hide
    });

    return (
      <button className={classes}
              onClick={this.on_click} >
        Hide Completed
      </button>
    );
  }

});





var ListControlBar = React.createClass({

  _onHideToggle: function(hidden)
  {
    console.log("_onHideToggle() --hidden---> ", hidden);
    this.props.onHideCompleted(hidden);
  },


  render: function()
  {
    return (
      <div className="list-control-bar">
        <ItemCountWidget itemCount={this.props.items.length} />
        <HideCompletedButton onHideToggle={this._onHideToggle} />
      </div>
    );
  }

});




module.exports = ListControlBar;

