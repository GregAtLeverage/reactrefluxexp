/**
 * Created by goat on 2015-02-28.
 */

var keyMirror = require('react/lib/keymirror');

module.exports = keyMirror({
  GROCERY_LIST_CREATE: null,
  GROCERY_LIST_SET_COMPLETE: null,
  GROCERY_LIST_DESTROY: null,
  GROCERY_LIST_DESTROY_COMPLETED: null,
  GROCERY_LIST_TOGGLE_COMPLETE_ALL: null,
  GROCERY_LIST_UNDO_COMPLETE: null,
  GROCERY_LIST_UPDATE_ITEM: null,
  GROCERY_LIST_LOAD_FROM_STORAGE: null
});
