/**
 * Created by goat on 2015-02-28.
 */


var StorageObject = {};
var STORAGE_KEY = "grocery_list_store";


var Storage = (function(){
  var pblc = {},
      prvt = {};

  var storage = window.localStorage;

  prvt.loadStore = function()
  {
    var _sobj = storage.getItem(STORAGE_KEY);
    if(typeof _sobj === "string"){
      var decoded = JSON.parse(_sobj);
      if(typeof decoded === "object"){
        StorageObject = decoded;
      }
    }
  };

  prvt.saveStore = function()
  {
    var encoded = JSON.stringify(StorageObject);
    storage.setItem(STORAGE_KEY, encoded);
  }

  pblc.getStore = function()
  {
    return StorageObject;
  }

  pblc.set = function(id, value)
  {
    StorageObject[id] = value;
    prvt.saveStore();
  }

  pblc.unset = function(id)
  {
    if(StorageObject && StorageObject[id]){
      delete StorageObject[id];
      prvt.saveStore();
    }
  }

  pblc.get = function(id)
  {
    if(StorageObject && StorageObject[id]){
      return StorageObject[id];
    }
    return null;
  }

  prvt.loadStore();
  return pblc;
})();



module.exports = Storage;

