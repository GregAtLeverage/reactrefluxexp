/**
 * Created by goat on 2015-02-28.
 */

var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var GroceryListConstants = require('../constants/GroceryListConstants');
var assign = require('react/lib/object.assign');

var TodoActions = require('../actions/GroceryListActions');

var storage = require('../storage');

var CHANGE_EVENT = 'change';

var _groceries = {};









function loadFromStorage(data)
{
  var groc_obj = false;
  for(var id in data){
    groc_obj = data[id];
    _groceries[id] = groc_obj;
  }
}



function create(text) {
  var id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
  _groceries[id] = {
    id: id,
    complete: false,
    text: text
  };

  storage.set(id, _groceries[id]);
}



function update(id, updates) {
  _groceries[id] = assign({}, _groceries[id], updates);
  storage.set(id, _groceries[id]);
}



function updateAll(updates) {
  for (var id in _groceries) {
    update(id, updates);
  }
}



function destroy(id) {
  delete _groceries[id];
  storage.unset(id);
}



function destroyCompleted() {
  for (var id in _groceries) {
    if (_groceries[id].complete) {
      destroy(id);
    }
  }
}







var GroceryListStore = assign({}, EventEmitter.prototype, {

  areAllComplete: function() {
    for (var id in _todos) {
      if (!_groceries[id].complete) {
        return false;
      }
    }
    return true;
  },


  getAll: function() {
    return _groceries;
  },


  getAllArray: function()
  {
    var arr = [];
    for(var key in _groceries){
      arr.push(_groceries[key]);
    }
    return arr;
  },


  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },


  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },


  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }

});






// Register callback to handle all updates
AppDispatcher.register(function(action) {
  var text;
  var data;

  switch(action.actionType) {

    case GroceryListConstants.GROCERY_LIST_LOAD_FROM_STORAGE:
      console.log("LOAD FROM STORAGE... ", action.data);
      data = action.data;
      loadFromStorage(data);
      GroceryListStore.emitChange();
      break;


    case GroceryListConstants.GROCERY_LIST_CREATE:
      text = action.text.trim();
      if (text !== '') {
        create(text);
      }
      GroceryListStore.emitChange();
      break;

    case GroceryListConstants.GROCERY_LIST_TOGGLE_COMPLETE_ALL:
      if (GroceryListStore.areAllComplete()) {
        updateAll({complete: false});
      } else {
        updateAll({complete: true});
      }
      GroceryListStore.emitChange();
      break;


    case GroceryListConstants.GROCERY_LIST_SET_COMPLETE:
      console.log("STORE ==SET_COMPLETE===> ", action);
      update(action.id, {complete: action.complete});
      GroceryListStore.emitChange();
          break;

    case GroceryListConstants.GROCERY_LIST_UPDATE_ITEM:
      console.log("STORE ==UPDATE_ITEM===> ", action);
      text = action.text.trim();
      if (text !== '') {
        update(action.id, {text: text});
      }
      GroceryListStore.emitChange();
      break;

    case GroceryListConstants.GROCERY_LIST_DESTROY:
      destroy(action.id);
      GroceryListStore.emitChange();
      break;

    case GroceryListConstants.GROCERY_LIST_DESTROY_COMPLETED:
      destroyCompleted();
      GroceryListStore.emitChange();
      break;

    default: break;
  }
});




module.exports = GroceryListStore;

